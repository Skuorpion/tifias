#Made by Xzing, project Tifias
extends KinematicBody2D

#A crate wich can be pushed by the player

const up =  Vector2(0,-1)
const gravity = 300
var vel = Vector2()
var mvfe

func _ready():
	vel.y = gravity

func _physics_process(delta):
	mvfe=move_and_slide(vel, up)
	if(vel.x):
		decrement()
	mvfe = delta

func _on_Right_area_body_entered(body):
	vel.x = -512
	mvfe = body

func _on_Left_area_body_entered(body):
	vel.x = 512
	mvfe = body

func decrement(): #the crate's speed slowly goes down
	if (vel.x<-75):
		vel.x+=75
	elif (vel.x>75):
		vel.x-=75
	else:
		vel.x=0