#Made by Xzing, project Tifias
extends Area2D

#a zone wich detects if the player is here and transmit it to all it's childs

signal is_active
signal is_innactive
var mvfe

func _ready():
	var nb_childs = get_child_count()-1
	for index in range (nb_childs) :
		mvfe=self.connect("is_active",get_child(index+1),"activate")
		mvfe=self.connect("is_innactive",get_child(index+1),"unactivate")

func _on_Zone_detector_body_entered(body):
	self.emit_signal("is_active")
	mvfe = body

func _on_Zone_detector_body_exited(body):
	self.emit_signal("is_innactive")
	mvfe = body