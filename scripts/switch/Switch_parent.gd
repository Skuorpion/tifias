#Made by Xzing, project Tifias
extends Node

#The parent node of switchs

signal trigger(color)

var green
var mvfe

func _ready():
	var nb_childs = get_child_count()
	for index in range (nb_childs) :
		mvfe=self.connect("trigger",get_child(index),"up_trigger")
	green = true

func switch_trigger():
	self.emit_signal("trigger",green)
	if (green) : #if true, means that green blocks are going to turn non-solid
		green=false
	else :
		green=true