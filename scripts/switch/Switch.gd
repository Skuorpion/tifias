#Made by Xzing, project Tifias
extends StaticBody2D

#When to layer step on it, switch the state of all switch blocks

signal switch_trigger

var cooldown=0
var mvfe

func _ready():
	mvfe=self.connect("switch_trigger",get_parent(),"switch_trigger") #need a switch_parent
	pass

func _physics_process(delta):
	if (cooldown>0):
		cooldown-=delta
	elif (cooldown<0):
		cooldown=0

func _on_Area2D_body_entered(body):
	if(!cooldown):
		self.emit_signal("switch_trigger")
		cooldown=0.5
	mvfe=body

func change_sprite(active_frame):
	if (active_frame): #if green
		$Sprite.frame+=1
	else : #if white
		$Sprite.frame-=1

func up_trigger(green_state):
	change_sprite(green_state)