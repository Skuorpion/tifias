#Made by Xzing, project Tifias
extends Node2D #1.2

#The root node of the level : check if the level is finished and allow the link with the following level

var timer = 0
export (String) var niveau_suivant
var mvfe

func _physics_process(delta): #each second, the finish status is checked
	if(Globals_1_2.is_playing):
		check_end(delta)

func check_end(delta):
	if (timer<0.3):
		timer+= delta
	else :
		timer = 0
		if (Globals_1_2.is_level_completed): #means that level remains finished for the last 0.3 seconds
			change_level()
		else :
			Globals_1_2.is_level_completed = true

func change_level():
	Globals_1_2.is_playing = false
	mvfe = get_tree().change_scene(niveau_suivant)