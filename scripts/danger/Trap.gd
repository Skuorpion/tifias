#Made by Xzing, project Tifias
extends Area2D

#Looks like the wall_spikes but is lethal only for a certain amount of time
#Need a specific tree : zone_detecor as parent and danger_parent as parent_parent

signal death

#all in seconds
export (float) var period #period is the non-lethal time
export (float) var lethal_time 
export (float) var initial_delay #applies once

var mvfe
var timer = 0
var lethal = false
var player_colliding = false
var active = false #for optimisation

func _ready():
	mvfe=self.connect("death",get_parent().get_parent(),"death_trigger")

func _physics_process(delta):
	if(active):
		timer+=delta
		if (lethal):
			kill_test()
		test_swap()

func kill_test():
	if(player_colliding):
		self.emit_signal("death")
		player_colliding = false #avoid instant-kill when respawn

func test_swap():
	if (lethal):
		if (timer > lethal_time):
			swap_state()
	else :
		if (timer > period):
			swap_state()

func swap_state():
	timer = 0
	lethal = !(lethal)
	if(lethal):
		$Sprite.frame = 54
	else :
		$Sprite.frame = 53

func _on_Trap_body_entered(body):
	player_colliding = true
	mvfe = body

func _on_Trap_body_exited(body):
	player_colliding = false
	mvfe = body

func activate():
	active = true
	timer = -(initial_delay)

func unactivate():
	active = false
	lethal = false
	$Sprite.frame = 53