#Made by Xzing, project Tifias
extends Area2D

#A death water with an animation

signal death

var mvfe

func _ready():
	mvfe=self.connect("death",get_parent(),"death_trigger") #need a death_parent
	$Anim.play("PWater")
	pass

func _on_PWater_body_entered(body):
	self.emit_signal("death")
	mvfe=body