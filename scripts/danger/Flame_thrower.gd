#Made by Xzing, project Tifias
extends Area2D

#Emit Flames using some parameters

enum orientation_enum {up, right, down, left} #0,1,2,3
export(orientation_enum) var orientation
export (float) var tempo # 0 if the flame is constant
export (int) var fire_range #the range of the flame (/32)

var fire = preload("res://prefabs/danger/flame.tscn")
var active = false #for optimisation, is active only if the player is near
var timer = 0
var state = 0
var dir = Vector2()
var mvfe

func _ready():
	gest_orientation()
	gest_sprite()

func gest_orientation():
	if (orientation%2==0): #up or down
		if(orientation==0): #up
			dir.y=-1
		else: #down
			dir.y=1
	else: #left or right
		if(orientation==3): #left
			dir.x=-1
		else: #right
			dir.x=1

func gest_sprite():
	if (orientation%2==0): #same as gest_orientation
		if(orientation==0):
			$Sprite.rotation_degrees = -90
		else :
			$Sprite.rotation_degrees = 90
	else:
		if(orientation==3):
			$Sprite.rotation_degrees = 180

func _physics_process(delta):
	if(active):
		if (tempo == 0):
			generate_column()
		else :
			timer+=delta
			if (timer>tempo):
				genarate_fire()

func generate_column():
	if(!state):
		while (state != fire_range):
			create_fire()
			state+=1

func genarate_fire():
	if (state != fire_range):
		create_fire()
		state+=1
	else:
		child_abort()

func create_fire():
	var flamme = fire.instance()
	flamme.init(dir.x, dir.y, test_head(), tempo, (fire_range-state-1) )
	self.add_child(flamme)
	timer = 0

func test_head():
	if(state):
		return false
	return true

func child_abort():
	state = 0
	var nb_childs = (get_child_count()-3) #because there are 3 original childrens
	for index in range (nb_childs) :
		get_child(index+3).queue_free()

func _on_Area2D_body_entered(body):
	active = true
	mvfe = body

func _on_Area2D_body_exited(body):
	active = false
	child_abort()
	mvfe = body