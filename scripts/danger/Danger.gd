#Made by Xzing, project Tifias
extends Area2D

#Basic script for danger : send death signal to parent node when body_entered

signal death

var mvfe

func _ready():
	mvfe=self.connect("death",get_parent(),"death_trigger") #Need a death_parent node
	pass

func _on_Spikes_body_entered(body):
	self.emit_signal("death")
	mvfe=body